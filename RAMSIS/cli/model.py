import typer
from datetime import datetime
from rich import print
from rich.table import Table
from marshmallow import EXCLUDE
from RAMSIS.db import db_url, session_handler, init_db
from ramsis.datamodel import ModelConfig
import json
from ramsis.io.configuration import ModelConfigurationSchema
from pathlib import Path

from sqlalchemy import select


app = typer.Typer()


@app.command()
def ls(help="Outputs list of models"):
    with session_handler(db_url) as session:
        model_configs = session.execute(
            select(ModelConfig)).scalars().all()
        for model in model_configs:
            table = Table(show_footer=False,
                          title=f"Model Config {model.name}",
                          title_justify="left")
            table.add_column("attribute")
            table.add_column("value")
            for attr in ModelConfig.__table__.columns:
                table.add_row(str(attr.name), str(getattr(model, attr.name)))

            print(table)


@app.command()
def disable(
        model_name: str,
        help="Set enabled=False. This model will not run, even if the same "
        "tags are set on the forecast series."):
    with session_handler(db_url) as session:
        model_config = session.execute(
            select(ModelConfig).filter_by(
                name=model_name)).\
            scalar_one_or_none()
        if not model_config:
            print("Model does not exist")
            raise typer.Exit()
        model_config.enabled = False
        session.commit()


@app.command()
def enable(
        model_name: str,
        help="Set enabled=True. This model will run if the same tags are set"
        "on the forecast series."):
    with session_handler(db_url) as session:
        model_config = session.execute(
            select(ModelConfig).filter_by(
                name=model_name)).\
            scalar_one_or_none()
        if not model_config:
            print("Model does not exist")
            raise typer.Exit()
        model_config.enabled = True
        session.commit()


@app.command()
def archive(
        model_name: str,
        force: bool = typer.Option(
            False, help="Force the deletes without asking")):
    """Appends timestamp to the name of model and enabled=False.
    For use in cases when you change the model config/code and want to
    archive the results. This model will no longer be run if not enabled.
    """
    with session_handler(db_url) as session:
        model_config = session.execute(
            select(ModelConfig).filter_by(
                name=model_name)).\
            scalar_one_or_none()
        if not model_config:
            print("Model does not exist")
            raise typer.Exit()
        if 'ARCHIVED' not in model_config.name:
            model_config.name = (f"{model_config.name}-ARCHIVED-"
                                 f"{datetime.now().strftime('%Y-%m-%d')}")
            model_config.enabled = False
        session.commit()
        print(f"Model successfully archived: {model_config.name}")


@app.command()
def archive_all(
        force: bool = typer.Option(
            False, help="Force the deletes without asking")):
    """Appends timestamp to the name of all models and enabled=False.
    For use in cases when you change the model config/code and want to
    archive the results. This model will no longer be run if not enabled.
    """
    with session_handler(db_url) as session:
        model_configs = session.execute(
            select(ModelConfig)).\
            scalars().all()
        if not model_configs:
            print("Model does not exist")
            raise typer.Exit()
        for model in model_configs:
            if 'ARCHIVED' not in model.name:
                model.name = (f"{model.name}-ARCHIVED-"
                              f"{datetime.now().strftime('%Y-%m-%d')}")
                model.enabled = False
        session.commit()
        print("All models successfully archived: "
              f"{[model.name for model in model_configs]}")


@app.command()
def load(
        model_config: Path = typer.Option(
        ...,
        exists=True, readable=True, help=(
            "Path to model config containing Seismicity Model config"))):

    success = init_db(db_url)

    if success:
        pass
    else:
        print(f"Error, db could not be initialized: {success}")
        raise typer.Exit()
    with session_handler(db_url) as session:
        with open(model_config, "r") as model_read:
            config_dict = json.load(model_read)

        for config in config_dict["model_configs"]:
            existing_config = session.execute(
                select(ModelConfig).filter_by(
                    name=config["name"])).\
                scalar_one_or_none()

            if existing_config:
                print(f"Model config exists for {existing_config.name}.")
                # Check if there are already completed forecasts
                # associated before allowing modification.
                print(existing_config.runs, "existing runs")
                if existing_config.runs:
                    session.rollback()
                    print("Model runs already exist for this config"
                          " Please upload the config with a new name.")
                    raise typer.Exit(code=1)
                print("deleting existing config")
                session.delete(existing_config)
                new_model_config = ModelConfigurationSchema(
                    unknown=EXCLUDE, context={"session": session}).load(config)
                new_model_config.id = existing_config.id
            else:
                new_model_config = ModelConfigurationSchema(
                    unknown=EXCLUDE, context={"session": session}).load(config)
                print("Model config is being added for "
                      f"{new_model_config.name}")

            session.add(new_model_config)
            session.commit()
            print("A model has been configured with the name: "
                  f"{new_model_config.name}, id: {new_model_config.id}")
